require('dotenv').config();
const MPPCLONE_TOKEN = process.env.MPPCLONE_TOKEN;
const { Bot } = require('./src/bot');
const bot = new Bot(MPPCLONE_TOKEN);
