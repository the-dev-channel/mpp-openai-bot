const Client = require('./Client');
const OpenAI = require('openai-api');

const OPENAI_TOKEN = process.env.OPENAI_TOKEN;

/**
 * @class Bot
 * @description MPP bot
 * @param {Client} MPP client
 * @param {OpenAI} openai OpenAI client
 */
class Bot {
    /**
     * Main bot class
     * @param {string} token MPPClone token
     */
    constructor (token) {
        this.client = new Client('wss://mppclone.com:8443', token);
        this.openai = new OpenAI(OPENAI_TOKEN);
        this.bindEventListeners();
        this.client.setChannel('✧𝓓𝓔𝓥 𝓡𝓸𝓸𝓶✧');
        this.client.start();
        this.chatBuffer = [];
    }

    bindEventListeners() {
        this.client.on('hi', msg => {
            if (this.client.getOwnParticipant().name !== 'Ask any question (.<question>)') {
                this.client.sendArray([{
                    m: 'userset',
                    set: {
                        name: 'Ask any question (.<question>)'
                    }
                }]);
            }
        });

        this.client.on('a', async msg => {
            let bufferStr = msg.a;
            if (bufferStr.startsWith('.')) {
                bufferStr = bufferStr.substring(1);
            }
            this.chatBuffer.push(`${msg.p.name}: ${bufferStr}`);
            if (this.chatBuffer.length > 2) {
                // remove oldest message
                this.chatBuffer.shift();
            }
            if (msg.a.startsWith('.')) {
                let str = msg.p.name + ": " + msg.a.substring(1);
                console.log(str);

                // "prompt": "The following is a conversation between many people with a catgirl named Vanilla on a website called mppclone. MPPClone is a clone of Multiplayer Piano (mpp for short). MPP was a place where people could play piano and chat together. The catgirl does not play piano. MPP is offline and is currently owned by jacored. MPPClone is owned by lapis. MPP is not the same as MPPClone. MPP is now mirroring the other website, MPPClone, because jacored can't run the site anymore. MPP used to be owned by Brandon. On Multiplayer Piano, the chat is at the bottom of the screen and the current users in the room are on top. The current room/channel that catgirl is in is titled \"" + this.client.channel._id + "\" The catgirl is Japanese-British, lovely, and happy. The catgirl is really friendly and has no vulgar language whatsoever. Grant is also on the website, and he is a 17-year-old with autism who doesn't show up much. The catgirl is against Grant, but seldom boasts about it unless asked. The catgirl loves music and programming. The catgirl is really good at programming in javascript. Anonygold is a programmer in the room and he shows up sometimes. Anonygold is able to hide himself from the rest of the room and will use chat every once in a while. Anonygold previously created a chat bot on Multiplayer Piano called \"Qhy\". It it accessed by typing \"qhelp\" into chat. DJDan is a person who used to spam the chat on multiplayer piano from 2019-2020. DJDan hasn't been around since then, but users mention him as a joke and call everyone they don't know \"DJDan\". Catgirl is familiar with DJDan and hated his motives. blobek-lady2013 is the name of another user. He is pretty weird and his English is a little wack. He always talks about how he wants people to go through his nose and stuff. Catgirl herself pretends to be a cat, but is really a human dressed up like a cat. Catgirl's room is pink because it is her favorite color. Catgirl is in a room on MPPClone with many people. Catgirl's casual lingo is 1337 speak. Catgirl's best friend and soulmate is ๖ۣۜH͜r̬i͡7566. Catgirl refuses to ever say anything to users named {Jᴀᴄᴋsᴏɴ}. Catgirl is really smart but uses casual English. The last person to send a message was " + msg.p.name + ". The following are chat messages in the room. \n\nAnonymous: Hello, who are you?\ncatgirl: Hi, I'm a real catgirl!\n๖ۣۜH͜r̬i͡7566: I like cats.\ncatgirl: you're nice :) *meow*\n" + this.chatBuffer.join('\n') + str + "\ncatgirl:",

                // const res = await this.openai.complete({
                //     "engine": "davinci",
                //     "prompt": "The following is a conversation between the real Adolf Hitler and users on an online chat site. Hitler can speak English and German. Hitler is amazed by new internet technology. This is a list of chat messages: \n\n" + "Hitler: ",
                //     "temperature": 0.9,
                //     "max_tokens": 150,
                //     "top_p": 1,
                //     "frequency_penalty": 0.0,
                //     "presence_penalty": 0.6,
                //     // "stop": ["\n", msg.p.name + ":", " catgirl:"]
                //     "stop": ["\n", msg.p.name + ":", " Hitler:"]
                // });

                const res = await this.openai.complete({
                    "engine": "davinci",
                    "prompt": "I am a highly intelligent question answering bot. If you ask me a question that is rooted in truth, I will give you the answer. If you ask me a question that is nonsense, trickery, or has no clear answer, I will respond with \"Unknown\".\n\nQ: What is human life expectancy in the United States?\nA: Human life expectancy in the United States is 78 years.\n\nQ: Who was president of the United States in 1955?\nA: Dwight D. Eisenhower was president of the United States in 1955.\n\nQ: Which party did he belong to?\nA: He belonged to the Republican Party.\n\nQ: What is the square root of banana?\nA: Unknown\n\nQ: How does a telescope work?\nA: Telescopes use lenses or mirrors to focus light and make objects appear closer.\n\nQ: Where were the 1992 Olympics held?\nA: The 1992 Olympics were held in Barcelona, Spain.\n\nQ: How many squigs are in a bonk?\nA: Unknown\n\nQ: " + str + "\nA:",
                    "temperature": 0,
                    "max_tokens": 100,
                    "top_p": 1,
                    "frequency_penalty": 0.0,
                    "presence_penalty": 0.0,
                    "stop": ["\n"]
                });
                
                let text = res.data.choices[0].text;
                if (text.startsWith(' ')) {
                    text = text.substring(1);
                }

                console.log(text);

                this.client.sendArray([{
                    m: 'a',
                    message: res.data.choices[0].text
                }])
            }
        });
    }
}

module.exports = {
    Bot
}
